package mt.um.edu.las3006.common.utils;

import mt.edu.um.las3006.assignment.security.KeyLoader;
import mt.edu.um.las3006.assignment.security.SignatureBuilder;
import mt.edu.um.las3006.assignment.security.SignatureVerifier;
import mt.um.edu.las3006.common.Constants;
import mt.um.edu.las3006.common.KeystoreFileCredentials;
import mt.um.edu.las3006.common.MessageType;
import mt.um.edu.las3006.common.exceptions.ErrorCodes;
import mt.um.edu.las3006.common.exceptions.GenericException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.function.BiFunction;

public class ChannelUtils {

    private static final Logger log = LoggerFactory.getLogger(ChannelUtils.class);

    public static void sendBytesToChannel(String messageToSend, SocketChannel channel) throws IOException {
        ByteBuffer buffer = ByteBuffer.wrap(messageToSend.getBytes());

        log.info("==> {}", messageToSend);

        buffer.put(messageToSend.getBytes());
        buffer.flip();

        while (buffer.hasRemaining()) {
            channel.write(buffer);
        }

        buffer.clear();
    }

    public static String readBytesFromChannel(ByteBuffer buffer, SocketChannel channel) throws IOException {

        StringBuilder messageReceived = new StringBuilder();
        int bytesRead;

        // While there are more bytes to read.
        while ((bytesRead = channel.read(buffer)) > 0) {
            byte[] arrayOfBytes = new byte[bytesRead];

            buffer.flip();
            buffer.get(arrayOfBytes);
            messageReceived.append(new String(arrayOfBytes));

            if (buffer.hasRemaining()) {
                buffer.compact();
            }
            else {
                buffer.clear();
                // base case, do another iteration if the buffer is full.
                if(bytesRead != buffer.limit()) {
                    log.info("<== {}", messageReceived.toString());
                    return messageReceived.toString();
                }
            }
        }

        // Close the channel socket if the bytes received is -1.
        if (bytesRead == -1) {
            Socket socket = channel.socket();
            SocketAddress remoteAddress = socket.getRemoteSocketAddress();
            log.info("Connection closed by client: {}", remoteAddress);
            channel.close();
        }

        return messageReceived.toString();
    }

    public static String signPayload(MessageType header, String body, KeystoreFileCredentials keystoreFileCredentials) throws GenericException
    {
        try {
            PrivateKey privateKey = KeyLoader
                    .loadPrivateKey(Paths.get(Constants.SECURITY_DIRECTORY,
                            keystoreFileCredentials.getKeystoreFilename()),
                            keystoreFileCredentials.getKeyAlias(),
                            keystoreFileCredentials.getPassword(),
                            keystoreFileCredentials.getKeystorePassword());

            return new SignatureBuilder(privateKey)
                    .addData(header == null ? "" : header.name())
                    .addData(body)
                    .sign();

        }catch (RuntimeException ex){
            throw new GenericException(ErrorCodes.UNKNOWN_PARTICIPANT);
        }
    }

    public static boolean verifyTransactionDetails(String[] bodyArray, String nodeName, String timestamp, String hash, String receivedNodeSignature){

        PublicKey nodePublicKey = KeyLoader.loadPublicKey(Paths.get(Constants.SECURITY_DIRECTORY,nodeName + ".crt"));


        return new SignatureVerifier(nodePublicKey)
                .addData(bodyArray[0]) //guid
                .addData(bodyArray[1]) // senderPublicKey
                .addData(bodyArray[2]) // recipientPublicKey
                .addData(bodyArray[3]) // amount
                .addData(bodyArray[4]) // sender signature
                .addData(timestamp) // timestamp
                .addData(hash) // transaction hash
                .addData(nodeName) // node name
                .verify(receivedNodeSignature);
    }

    public static final BiFunction<String, String[], Boolean> verifyNode = (nodeSignature, body) -> {
        String[] node = nodeSignature.split(":");
        boolean verifiedByNode = ChannelUtils.verifyTransactionDetails(body, node[0], body[5], body[6], node[1]);
        log.info("Node {} verified = {}", node[0], verifiedByNode);
        return verifiedByNode;
    };
}
