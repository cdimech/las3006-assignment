package mt.um.edu.las3006.common;

public class KeystoreFileCredentials {

    private final String keystoreFilename;

    private final String keyAlias;

    private final String password;

    private final String keystorePassword;

    public KeystoreFileCredentials(String keystoreFilename, String keyAlias, String password, String keystorePassword) {
        this.keystoreFilename = keystoreFilename;
        this.keyAlias = keyAlias;
        this.password = password;
        this.keystorePassword = keystorePassword;
    }

    public String getKeystoreFilename() {
        return keystoreFilename;
    }

    public String getKeyAlias() {
        return keyAlias;
    }

    public String getPassword() {
        return password;
    }

    public String getKeystorePassword() {
        return keystorePassword;
    }
}
