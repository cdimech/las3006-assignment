package mt.um.edu.las3006.common;

public class Constants {

    public static final String SECURITY_DIRECTORY = "security-resources";
    public static final String NODE_PROPERTIES_FILENAME = "nodes.properties";
    public static final String OUTGOING_SEPARATOR = "&";
    public static final String BODY_SEPARATOR = ",";
    public static final Long NODES_TO_APPROVE = 2L;


}
