package mt.um.edu.las3006.common.exceptions;

public class GenericException extends Exception {

    private ErrorCodes errorCodes;

    public GenericException(ErrorCodes errorCodes) {
        this.errorCodes = errorCodes;
    }

    public ErrorCodes getErrorCodes() {
        return errorCodes;
    }
}
