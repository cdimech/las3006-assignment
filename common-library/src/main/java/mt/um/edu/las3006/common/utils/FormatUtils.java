package mt.um.edu.las3006.common.utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class FormatUtils {

    public static String getFormattedAmount(Double amount) {
        DecimalFormat df = new DecimalFormat("#.000000");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(amount);
    }
}
