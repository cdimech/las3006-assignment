package mt.um.edu.las3006.common.domain;

import mt.edu.um.las3006.assignment.security.KeyLoader;
import mt.edu.um.las3006.assignment.security.SignatureBuilder;
import mt.um.edu.las3006.common.KeystoreFileCredentials;
import mt.um.edu.las3006.common.MessageType;
import mt.um.edu.las3006.common.builder.OutgoingMessageBuilder;
import mt.um.edu.las3006.common.exceptions.ErrorCodes;
import mt.um.edu.las3006.common.exceptions.GenericException;
import mt.um.edu.las3006.common.utils.ChannelUtils;
import mt.um.edu.las3006.common.utils.MessageDecoder;

import java.security.PrivateKey;
import java.security.PublicKey;

public class ClientMessage {

    private final MessageType header;

    private final String body;

    // user public key
    private final PublicKey bottom;

    private KeystoreFileCredentials keystoreFileCredentials;

    // signed payload
    private String footer;

    public ClientMessage(MessageType header, String body, PublicKey bottom, PrivateKey privateKey){
        this.header = header;
        this.body = body;
        this.bottom = bottom;
        this.footer = new SignatureBuilder(privateKey)
                .addData(header.name())
                .addData(body)
                .sign();
    }

    public ClientMessage(MessageType header, String body, PublicKey bottom, KeystoreFileCredentials keystoreFileCredentials) {
        this.header = header;
        this.body = body;
        this.bottom = bottom;
        this.keystoreFileCredentials = keystoreFileCredentials;
    }

    public ClientMessage(MessageType header, String body, PublicKey bottom, String footer) {
        this.header = header;
        this.body = body;
        this.bottom = bottom;
        this.footer = footer;
        this.keystoreFileCredentials = null;
    }

    public MessageType getHeader() {
        return header;
    }

    public String getBody() {
        return body;
    }

    public PublicKey getBottom() {
        return bottom;
    }

    public String getFooter() throws GenericException {
        this.footer = this.footer == null ?
                ChannelUtils.signPayload(header, body, keystoreFileCredentials) : footer;

        return footer;
    }

    public String getMessage() throws GenericException {
        return new OutgoingMessageBuilder(header.name())
                .addData(body)
                .addData(KeyLoader.encodePublicKey(bottom))
                .addData(getFooter())
                .build();
    }

    public static ClientMessage from(String message) throws GenericException {
        String[] outgoingMessage = MessageDecoder.decodeOutgoingMessage(message);
        if (outgoingMessage.length > 3) {
            try {
                PublicKey publicKey = KeyLoader.decodePublicKey(outgoingMessage[2]);
                return new ClientMessage(MessageType.valueOf(outgoingMessage[0]), outgoingMessage[1], publicKey, outgoingMessage[3]);
            }
            catch (Exception ex) {
                throw new GenericException(ErrorCodes.UNKNOWN_PARTICIPANT);
            }
        }
        else {
            throw new GenericException(ErrorCodes.INVALID_MESSAGE_BODY);
        }
    }
}
