package mt.um.edu.las3006.common.builder;

import mt.um.edu.las3006.common.Constants;

public class OutgoingMessageBuilder extends MessageBuilder {

    public OutgoingMessageBuilder() {
        super(Constants.OUTGOING_SEPARATOR);
    }

    public OutgoingMessageBuilder(String initialData) {
        super(Constants.OUTGOING_SEPARATOR, initialData);
    }
}
