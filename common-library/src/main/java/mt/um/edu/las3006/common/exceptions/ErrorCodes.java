package mt.um.edu.las3006.common.exceptions;

public enum ErrorCodes {

    SIGNATURE_NOT_VERIFIED(""),
    INVALID_TRANSACTION_AMOUNT("Invalid transaction amount."),
    INSUFFICIENT_FUNDS("Insufficient funds"),
    TRANSACTION_IN_PROGRESS("Transaction in progress, try again later."),
    VERIFICATION_FAILED("Verification failed"),
    UNKNOWN_PARTICIPANT("Unknown participant"),
    INVALID_MESSAGE_BODY("Invalid message body"),
    NO_MESSAGE_RECEIVED(""),
    NETWORK_ERROR(""),
    NODES_VERIFIED_NOT_ENOUGH("Not enough nodes connected with the host node");

    private String message;

    ErrorCodes(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
