package mt.um.edu.las3006.common;

public enum MessageType {

    BALANCE,
    BALANCE_RESP,
    BALANCE_ERR,
    HISTORY,
    HISTORY_RESP,
    HISTORY_ERR,
    AUTH,
    VERIFY,
    VERIFY_OK,
    VERIFY_ERR,
    AUTH_OK,
    AUTH_ERR,
    CONFIRM,
    RECORD,
    RECORD_OK,
    RECORD_ERR,
    CONFIRM_OK,
    CONFIRM_ERR,
    NONE_CONN;

}
