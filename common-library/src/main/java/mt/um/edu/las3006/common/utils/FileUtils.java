package mt.um.edu.las3006.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static mt.um.edu.las3006.common.Constants.NODE_PROPERTIES_FILENAME;
import static mt.um.edu.las3006.common.Constants.SECURITY_DIRECTORY;

/**
 * @author Claire Borg Dimech
 */
public class FileUtils
{

    private static final Logger log = LoggerFactory.getLogger(FileUtils.class);

    /**
     * Given a stream of string, split the lines using the '=' separator in order to obtain
     * a key/value pair for each line.
     *
     * @return a Map of node name and IP.
     */
    public static Map<String, String> readIpsFromFile(){
        try (Stream<String> lines = Files.lines(Paths.get(SECURITY_DIRECTORY, NODE_PROPERTIES_FILENAME))) {
            return lines
                    .map(line -> line.split("="))
                    .collect(Collectors.toMap(e -> e[0], e -> e[1]));
        }
        catch (IOException ex) {
            log.error("Unable to load node properties file {} due to {}", NODE_PROPERTIES_FILENAME, ex.getMessage());
            return new HashMap<>();
        }
    }

    /**
     * Perform an IP lookup for the specified node name from the NODE_PROPERTIES_FILENAME file.
     *
     * @param nodeName the node that will be retrieved.
     *
     * @return an Optional<String> of the IP.
     */
    public static Optional<String> ipLookupByNode(String nodeName){
        return Optional.ofNullable(readIpsFromFile().get(nodeName));
    }
}
