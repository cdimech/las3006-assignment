package mt.um.edu.las3006.common.utils;

import mt.um.edu.las3006.common.Constants;

public class MessageDecoder {

    public static String[] decodeOutgoingMessage(String message) {
        return decodeMessage(Constants.OUTGOING_SEPARATOR, message);
    }

    public static String[] decodeBodyMessage(String message) {
        return decodeMessage(Constants.BODY_SEPARATOR, message);
    }

    private static String[] decodeMessage(String separator, String message) {
        if (message != null) {
            return message.split(separator);
        }
        else
            return new String[0];

    }
}
