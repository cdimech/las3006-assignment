package mt.um.edu.las3006.common.builder;

import mt.um.edu.las3006.common.Constants;

public class BodyBuilder extends MessageBuilder {

    public BodyBuilder() {
        super(Constants.BODY_SEPARATOR);
    }

    public BodyBuilder(String initialData) {
        super(Constants.BODY_SEPARATOR, initialData);
    }
}
