package mt.um.edu.las3006.common.builder;

import java.util.Arrays;
import java.util.List;

public abstract class MessageBuilder {

    private StringBuilder stringBuilder;

    private final String separator;

    public MessageBuilder(String separator) {
        this.separator = separator;
    }

    public MessageBuilder(String separator, String initialData) {
        stringBuilder = new StringBuilder(initialData);
        this.separator = separator;
    }

    public MessageBuilder addData(String data) {
        stringBuilder.append(separator);
        stringBuilder.append(data);
        return this;
    }

    public List<String> from(String incoming) {
        return Arrays.asList(incoming.split(separator));
    }

    public String build() {
        return stringBuilder.toString();
    }
}
