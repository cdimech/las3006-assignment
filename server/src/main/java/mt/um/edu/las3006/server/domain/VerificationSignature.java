package mt.um.edu.las3006.server.domain;

public class VerificationSignature {

    private final String nodeName;

    private final String signature;

    public VerificationSignature(String verificationSignature){
        String[] verificationSignatureArray = verificationSignature.split(":");
        this.nodeName = verificationSignatureArray[0];
        this.signature = verificationSignatureArray[1];
    }
    public VerificationSignature(String nodeName, String signature) {
        this.nodeName = nodeName;
        this.signature = signature;
    }

    public String getNodeName() {
        return nodeName;
    }

    public String getSignature() {
        return signature;
    }

    @Override
    public String toString() {
        return String.format("%s:%s", nodeName, signature);
    }
}
