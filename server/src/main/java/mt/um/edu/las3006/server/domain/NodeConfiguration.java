package mt.um.edu.las3006.server.domain;

import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.Semaphore;

/**
 * Created by clairedimech on 04/01/2018.
 */
public class NodeConfiguration
{

    private Semaphore semaphore;

    private final SocketChannel channel;

    private final ByteBuffer buffer;

    private final SocketAddress remoteSocket;

    public NodeConfiguration(SocketChannel channel, ByteBuffer buffer, SocketAddress remoteSocket)
    {
        this.channel = channel;
        this.buffer = buffer;
        this.remoteSocket = remoteSocket;
        this.semaphore = new Semaphore(1);
    }

    public SocketChannel getChannel()
    {
        return channel;
    }

    public ByteBuffer getBuffer()
    {
        return buffer;
    }

    public SocketAddress getRemoteSocket()
    {
        return remoteSocket;
    }

    @Override
    public String toString()
    {
        return "NodeConfiguration{" +
                "channel=" + channel +
                ", buffer=" + buffer +
                ", remoteSocket=" + remoteSocket +
                '}';
    }

    public boolean trySend() {
        return semaphore.tryAcquire();
    }

    public void release() {
        semaphore.release();
    }
}
