package mt.um.edu.las3006.server.request;

import mt.edu.um.las3006.assignment.security.SignatureVerifier;
import mt.um.edu.las3006.common.MessageType;
import mt.um.edu.las3006.common.domain.ClientMessage;
import mt.um.edu.las3006.common.exceptions.ErrorCodes;
import mt.um.edu.las3006.common.exceptions.GenericException;
import mt.um.edu.las3006.server.domain.Node;
import mt.um.edu.las3006.server.domain.Transaction;
import mt.um.edu.las3006.server.utils.LedgerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.PublicKey;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class BaseRequest {

    protected final Logger log = LoggerFactory.getLogger(BaseRequest.class);

    protected final MessageType successResponseMessage;

    protected final MessageType errorResponseMessage;

    protected MessageType header;

    protected String body;

    protected PublicKey publicKey;

    protected String signedPayload;

    protected final Node node;

    protected final String rawRequest;

    protected volatile boolean completedSuccessfully = false;

    private AtomicBoolean verified = new AtomicBoolean(false);

    public BaseRequest(String request, Node node, MessageType successResponseMessage, MessageType errorResponseMessage) throws GenericException {
        this.rawRequest = request;
        this.node = node;
        this.successResponseMessage = successResponseMessage;
        this.errorResponseMessage = errorResponseMessage;

        ClientMessage clientMessage = ClientMessage.from(request);

        this.header = clientMessage.getHeader();
        this.body = clientMessage.getBody();
        this.publicKey = clientMessage.getBottom();
        this.signedPayload = clientMessage.getFooter();

        this.verified.set(verify());

        log.debug("Verified = {}.", verified);
    }

    protected boolean verify() {
        return new SignatureVerifier(publicKey)
                .addData(header.name())
                .addData(body)
                .verify(signedPayload);
    }

    protected abstract String computeResponse(List<Transaction> transactions) throws GenericException;

    public String verifyAndCompute() throws GenericException {
        List<Transaction> transactions = LedgerUtils.getTransactions(getNodeName());

        MessageType responseHeader = errorResponseMessage;
        String responseBody = null;

        try {

            if (isVerified()) {
                String response = computeResponse(transactions);

                if (completedSuccessfully) {
                    responseHeader = successResponseMessage;
                    responseBody = response;
                }
            }
            else {
                responseBody = ErrorCodes.VERIFICATION_FAILED.getMessage();
            }
        }
        catch (GenericException ex) {
            responseBody = ex.getErrorCodes().getMessage();
        }
        finally {
            ClientMessage clientMessage = new ClientMessage(responseHeader,
                    responseBody,
                    node.loadPublicKey(),
                    node.loadPrivateKey());

            return clientMessage.getMessage();
        }
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public Node getNode() {
        return node;
    }

    public String getNodeName() {
        return node.getName();
    }

    public boolean isVerified() {
        return this.verified.get();
    }

    public String getRawRequest() {
        return rawRequest;
    }

    public String getBody()
    {
        return body;
    }

    public String getSignedPayload()
    {
        return signedPayload;
    }
}
