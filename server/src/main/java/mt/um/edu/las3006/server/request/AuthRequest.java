package mt.um.edu.las3006.server.request;

import mt.um.edu.las3006.common.MessageType;
import mt.um.edu.las3006.common.builder.BodyBuilder;
import mt.um.edu.las3006.common.domain.ClientMessage;
import mt.um.edu.las3006.common.exceptions.ErrorCodes;
import mt.um.edu.las3006.common.exceptions.GenericException;
import mt.um.edu.las3006.common.utils.ChannelUtils;
import mt.um.edu.las3006.common.utils.MessageDecoder;
import mt.um.edu.las3006.server.domain.Node;
import mt.um.edu.las3006.server.domain.NodeConfiguration;
import mt.um.edu.las3006.server.domain.Transaction;
import mt.um.edu.las3006.server.domain.VerificationSignature;
import mt.um.edu.las3006.server.utils.LedgerUtils;
import mt.um.edu.las3006.server.utils.TransactionDetails;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class AuthRequest extends BroadcastRequest {

    public AuthRequest(String messageFromChannel, Node node, Map<String, NodeConfiguration> connectedNodes) throws GenericException {
        super(messageFromChannel, node, MessageType.AUTH_OK, MessageType.AUTH_ERR, connectedNodes, MessageType.VERIFY_OK, MessageType.VERIFY_ERR);
    }

    @Override
    protected String computeResponse(List<Transaction> transactions) throws GenericException
    {
        String verifiedTransaction;
        String[] bodyArray = MessageDecoder.decodeBodyMessage(getBody());

        if(bodyArray.length > 4)
        {
            TransactionDetails transactionDetails = LedgerUtils.verifyTransactionAndSign(bodyArray, transactions, publicKey, node, getSignedPayload());

            verifiedTransaction = new BodyBuilder(getBody())
                    .addData(String.valueOf(transactionDetails.getTimestamp()))
                    .addData(transactionDetails.getHashValue())
                    .addData(node.getName())
                    .addData(transactionDetails.getNodeSignature())
                    .build();

            ClientMessage clientMessage = new ClientMessage(MessageType.VERIFY,
                    verifiedTransaction,
                    node.loadPublicKey(),
                    node.loadPrivateKey());

            String message = clientMessage.getMessage();

            log.info("Message to broadcast to all nodes {}.", message);

            List<CompletableFuture<String>> completableFutures = broadcast(message);

            List<VerificationSignature> verificationSignatures = new ArrayList<>();

            completableFutures.forEach(stringCompletableFuture -> {
                try {

                    String[] responseFromNodes = MessageDecoder.decodeOutgoingMessage(stringCompletableFuture.get());
                    String[] nodeBodyArray = MessageDecoder.decodeBodyMessage(responseFromNodes[1]);

                    String nodeName = nodeBodyArray[9];
                    String timestamp = nodeBodyArray[10];
                    String hash = nodeBodyArray[11];
                    String receivedNodeSignature = nodeBodyArray[12];

                    boolean verified = ChannelUtils.verifyTransactionDetails(nodeBodyArray, nodeName, timestamp, hash, receivedNodeSignature);

                    if (verified) {
                        log.info("Verified by {}.", nodeName);
                        VerificationSignature verificationSignature = new VerificationSignature(nodeName, receivedNodeSignature);
                        verificationSignatures.add(verificationSignature);
                    }
                    else {
                        // do nothing, ignore response/node.
                    }
                }
                catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            });

            completedSuccessfully = true;

            verifiedTransaction = new BodyBuilder(getBody())
                    .addData(String.valueOf(transactionDetails.getTimestamp()))
                    .addData(transactionDetails.getHashValue())
                    .addData(node.getName() + ":" + transactionDetails.getNodeSignature())
                    .addData(verificationSignatures.get(0).toString())
                    .addData(verificationSignatures.get(1).toString())
                    .build();

            return verifiedTransaction;
        }
        else
        {
            throw new GenericException(ErrorCodes.INVALID_MESSAGE_BODY);
        }
    }
}
