package mt.um.edu.las3006.server.utils;

import mt.edu.um.las3006.assignment.security.HashBuilder;
import mt.edu.um.las3006.assignment.security.KeyLoader;
import mt.edu.um.las3006.assignment.security.SignatureBuilder;
import mt.um.edu.las3006.common.Constants;
import mt.um.edu.las3006.common.exceptions.ErrorCodes;
import mt.um.edu.las3006.common.exceptions.GenericException;
import mt.um.edu.las3006.common.utils.FormatUtils;
import mt.um.edu.las3006.server.domain.Node;
import mt.um.edu.las3006.server.domain.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LedgerUtils {

    private static final Logger log = LoggerFactory.getLogger(LedgerUtils.class);

    public static List<Transaction> getTransactions(String nodeName) {
        String ledgerFileName = nodeName + ".ledger";
        List<Transaction> transactions = new ArrayList<>();

        try (Stream<String> lines = Files.lines(Paths.get(Constants.SECURITY_DIRECTORY,ledgerFileName))) {

            lines
                    .parallel()
                    .map(line -> line.split(","))
                    .collect(Collectors.toList())
                    .forEach(item ->
                    {
                        Transaction transaction = new Transaction(Long.valueOf(item[0]),
                                item[1],
                                item[2],
                                KeyLoader.decodePublicKey(item[3]),
                                KeyLoader.decodePublicKey(item[4]),
                                item[5],
                                item[6],
                                item[7],
                                item[8],
                                item[9],
                                item[10]);
                        transactions.add(transaction);
                    });

            log.debug("Loaded {} transactions from {}.", transactions.size(), ledgerFileName);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return transactions;
    }

    public static Optional<Transaction> getLastTransactionByPublicKey(List<Transaction> transactions, PublicKey publicKey){
        return transactions
                .stream()
                .filter(transaction -> transaction.getRecipientPublicKey().equals(publicKey) || transaction.getSenderPublicKey().equals(publicKey))
                .sorted(Comparator.reverseOrder())
                .findFirst();
    }

    public static Double getBalance(List<Transaction> transactions, PublicKey publicKey){

        Double incomingBalance = transactions
                .stream()
                .filter(transaction -> transaction.getRecipientPublicKey().equals(publicKey))
                .map(Transaction::getAmount)
                .mapToDouble(Double::parseDouble)
                .sum();

        Double outgoingBalance = transactions
                .stream()
                .filter(transaction -> transaction.getSenderPublicKey().equals(publicKey))
                .map(Transaction::getAmount)
                .mapToDouble(Double::parseDouble)
                .sum();

        return incomingBalance - outgoingBalance;
    }

    /**
     * @param
     * @return
     * @throws GenericException
     */
    public static TransactionDetails verifyTransactionAndSign(String[] bodyArray, List<Transaction> transactions, PublicKey publicKey, Node node, String signedPayload) throws GenericException {

        String guid = bodyArray[0];
        PublicKey senderPublicKey = KeyLoader.decodePublicKey(bodyArray[1]);
        PublicKey recipientPublicKey = KeyLoader.decodePublicKey(bodyArray[2]);
        String senderSignature = bodyArray[4];

        // up to 6 decimal places
        final Double transactionAmount = Double.valueOf(bodyArray[3]);
        final Double senderBalance = LedgerUtils.getBalance(transactions, KeyLoader.decodePublicKey(bodyArray[1]));
        if (senderBalance.compareTo(transactionAmount) < 0)
        {
            log.error("Insufficient funds for sender {}.", bodyArray[1]);
            throw new GenericException(ErrorCodes.INSUFFICIENT_FUNDS);
        }

        final long timestamp = bodyArray.length > 5 ? Long.valueOf(bodyArray[5]) : System.currentTimeMillis();
        final String formattedAmount = FormatUtils.getFormattedAmount(transactionAmount);

        Optional<Transaction> lastSenderTransaction = LedgerUtils.getLastTransactionByPublicKey(transactions, senderPublicKey);

        Optional<Transaction> lastRecipientTransaction = LedgerUtils.getLastTransactionByPublicKey(transactions, recipientPublicKey);

        String hashTransaction = new HashBuilder(getTransactionHash(lastSenderTransaction))
                .addData(getTransactionHash(lastRecipientTransaction))
                .addData(guid)
                .addData(formattedAmount)
                .addData(bodyArray[4])
                .addData(String.valueOf(timestamp))
                .hash();

        if(bodyArray.length > 5){

            String existingHashValue = bodyArray[6];

            if (!existingHashValue.equalsIgnoreCase(hashTransaction)) {
                log.error("An error has occurred while comparing the existing hash with the new calculated hash.");
                throw new GenericException(ErrorCodes.VERIFICATION_FAILED);
            }

            log.debug("Hash values matched for {} and {}.", bodyArray[7], node.getName());
        }

        String nodeSignature = new SignatureBuilder(node.loadPrivateKey())
                .addData(guid)
                .addData(KeyLoader.encodePublicKey(senderPublicKey))
                .addData(KeyLoader.encodePublicKey(recipientPublicKey))
                .addData(formattedAmount)
                .addData(senderSignature)
                .addData(String.valueOf(timestamp))
                .addData(hashTransaction)
                .addData(node.getName())
                .sign();

        return new TransactionDetails(hashTransaction, nodeSignature, timestamp, formattedAmount);
    }

    private static String getTransactionHash(Optional<Transaction> transactionToHash){
        return transactionToHash
                .map(Transaction::getHash)
                .orElse(null);
    }

    public static void record(Transaction transaction, String nodeName) throws IOException {
        String nodeLedger = nodeName + ".ledger";

        Files.write(Paths.get(Constants.SECURITY_DIRECTORY, nodeLedger), transaction.toString().getBytes(), StandardOpenOption.APPEND);
    }
}
