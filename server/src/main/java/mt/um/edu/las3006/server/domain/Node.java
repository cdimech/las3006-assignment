package mt.um.edu.las3006.server.domain;

import mt.edu.um.las3006.assignment.security.KeyLoader;

import java.nio.file.Paths;
import java.security.PrivateKey;
import java.security.PublicKey;

public class Node {

    private static final String SECURITY_DIRECTORY = "security-resources";

    private final String name;

    private final int port;

    private final String keystorePassword;

    private final String keystoreFilename;

    public Node(String name, int port) {
        this.name = name;
        this.port = port;
        this.keystorePassword = name + "qwerty";
        this.keystoreFilename = name + ".pfx";
    }

    public String getName() {
        return name;
    }

    public int getPort() {
        return port;
    }

    public PrivateKey loadPrivateKey(){
        return KeyLoader.loadPrivateKey(Paths.get(SECURITY_DIRECTORY, keystoreFilename), name, keystorePassword, keystorePassword);
    }

    public PublicKey loadPublicKey(){
        return KeyLoader.loadPublicKey(Paths.get(SECURITY_DIRECTORY, keystoreFilename), name, keystorePassword);
    }

    public String getKeystorePassword() {
        return keystorePassword;
    }

    public String getKeystoreFilename() {
        return keystoreFilename;
    }
}
