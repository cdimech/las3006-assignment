package mt.um.edu.las3006.server.request;

import mt.edu.um.las3006.assignment.security.KeyLoader;
import mt.edu.um.las3006.assignment.security.SignatureVerifier;
import mt.um.edu.las3006.common.Constants;
import mt.um.edu.las3006.common.MessageType;
import mt.um.edu.las3006.common.builder.BodyBuilder;
import mt.um.edu.las3006.common.exceptions.GenericException;
import mt.um.edu.las3006.common.utils.MessageDecoder;
import mt.um.edu.las3006.server.domain.Node;
import mt.um.edu.las3006.server.domain.Transaction;
import mt.um.edu.las3006.server.utils.TransactionDetails;

import java.nio.file.Paths;
import java.util.List;

import static mt.um.edu.las3006.server.utils.LedgerUtils.verifyTransactionAndSign;

public class VerificationRequest extends BaseRequest {

    public VerificationRequest(String request, Node node) throws GenericException {
        super(request, node, MessageType.VERIFY_OK, MessageType.VERIFY_ERR);
    }

    /**
     * Overriding the implementation of verify for a VERIFY transaction since it requires custom validation.
     *
     * @return whether or not the signature has been verified.
     */
    @Override
    protected boolean verify() {

        if(body == null)
            return false;

        String[] bodyArray = MessageDecoder.decodeBodyMessage(body);

        String senderNodeCertificate = bodyArray[7] + ".crt";

        return new SignatureVerifier(KeyLoader.loadPublicKey(Paths.get(Constants.SECURITY_DIRECTORY, senderNodeCertificate)))
                .addData(bodyArray[0]) //guid
                .addData(bodyArray[1]) // senderPublicKey
                .addData(bodyArray[2]) // recipientPublicKey
                .addData(bodyArray[3]) // amount
                .addData(bodyArray[4]) // sender signature
                .addData(bodyArray[5]) // timestamp
                .addData(bodyArray[6]) // transaction hash
                .addData(bodyArray[7]) // node name
                .verify(bodyArray[8]);
    }

    @Override
    protected String computeResponse(List<Transaction> transactions) throws GenericException {
        try {
            TransactionDetails transactionDetails = verifyTransactionAndSign(MessageDecoder.decodeBodyMessage(getBody()), transactions, getPublicKey(), node, signedPayload);

            completedSuccessfully = true;

            return new BodyBuilder(getBody())
                    .addData(node.getName())
                    .addData(String.valueOf(transactionDetails.getTimestamp()))
                    .addData(transactionDetails.getHashValue())
                    .addData(transactionDetails.getNodeSignature())
                    .build();
        }
        catch (GenericException ex) {
            completedSuccessfully = false;
            throw ex;
        }
    }
}
