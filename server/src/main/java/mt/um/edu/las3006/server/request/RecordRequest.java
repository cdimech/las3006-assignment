package mt.um.edu.las3006.server.request;

import mt.um.edu.las3006.common.MessageType;
import mt.um.edu.las3006.common.exceptions.ErrorCodes;
import mt.um.edu.las3006.common.exceptions.GenericException;
import mt.um.edu.las3006.server.domain.Node;
import mt.um.edu.las3006.server.domain.Transaction;
import mt.um.edu.las3006.server.utils.LedgerUtils;

import java.io.IOException;
import java.util.List;

public class RecordRequest extends BaseRequest {

    public RecordRequest(String request, Node node) throws GenericException {
        super(request, node, MessageType.RECORD_OK, MessageType.RECORD_ERR);
    }

    @Override
    protected String computeResponse(List<Transaction> transactions) throws GenericException {
        try {

            Transaction transaction = Transaction.from(getBody());
            LedgerUtils.record(transaction, node.getName());

            completedSuccessfully = true;
        }
        catch (IOException ex) {
            throw new GenericException(ErrorCodes.NETWORK_ERROR);
        }
        return signedPayload;
    }
}
