package mt.um.edu.las3006.server;

import mt.um.edu.las3006.server.domain.NodeConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by clairedimech on 04/01/2018.
 */
public class NodeDiscovery
{
    private final Logger log = LoggerFactory.getLogger(NodeDiscovery.class);

    private final String nodeName;

    private final String host;

    private volatile ConcurrentMap<String, NodeConfiguration> nodeConfigurations;

    public NodeDiscovery(String nodeName, String host, ConcurrentMap<String, NodeConfiguration> nodeConfigurations)
    {
        this.nodeName = nodeName;
        this.host = host;
        this.nodeConfigurations = nodeConfigurations;
    }

    private synchronized void addToMap(String nodeName, NodeConfiguration configuration)
    {
        if(!this.nodeConfigurations.containsKey(nodeName))
        {
            this.nodeConfigurations.put(nodeName, configuration);
        }
    }

    synchronized void removeFromMap(String nodeName)
    {
        log.info("Removing {} from map.", nodeName);

        if(this.nodeConfigurations.containsKey(nodeName)){
            nodeConfigurations.remove(nodeName);
        }
    }

    /**
     * This is a blocking call on purpose, we want to wait until the node is available to connect to.
     *
     * @return whether or not the connection has been established successfully.
     */
    public Boolean call()
    {
        boolean channelConnected = false;

        // Only attempt to connect to it if there is no existing connection to this currentNode.
        if(!nodeConfigurations.containsKey(nodeName))
        {
            log.info("Attempting to connect to {}.", host);

            while(!channelConnected)
            {
                SocketChannel channel;

                try
                {
                    channel = SocketChannel.open();

                    String[] hostAndPort = host.split(":");

                    channelConnected = channel.connect(new InetSocketAddress(hostAndPort[0], Integer.valueOf(hostAndPort[1])));

                    if (channelConnected)
                    {
                        ByteBuffer buffer = ByteBuffer.allocate(1024);

                        NodeConfiguration nodeConfiguration = new NodeConfiguration(channel, buffer, channel.getRemoteAddress());
                        this.addToMap(nodeName, nodeConfiguration);

                        log.info("Successfully connected to {}.", host);
                    }
                } catch (IOException e)
                {
                    channelConnected = false;
                }
            }
        }

        return channelConnected;
    }

    @Override
    public String toString() {
        return String.format("%s : %s", nodeName, host);
    }
}
