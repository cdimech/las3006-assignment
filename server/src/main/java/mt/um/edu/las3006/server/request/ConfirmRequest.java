package mt.um.edu.las3006.server.request;

import mt.um.edu.las3006.common.Constants;
import mt.um.edu.las3006.common.KeystoreFileCredentials;
import mt.um.edu.las3006.common.MessageType;
import mt.um.edu.las3006.common.builder.BodyBuilder;
import mt.um.edu.las3006.common.domain.ClientMessage;
import mt.um.edu.las3006.common.exceptions.ErrorCodes;
import mt.um.edu.las3006.common.exceptions.GenericException;
import mt.um.edu.las3006.common.utils.ChannelUtils;
import mt.um.edu.las3006.common.utils.MessageDecoder;
import mt.um.edu.las3006.server.domain.Node;
import mt.um.edu.las3006.server.domain.NodeConfiguration;
import mt.um.edu.las3006.server.domain.Transaction;
import mt.um.edu.las3006.server.utils.LedgerUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class ConfirmRequest extends BroadcastRequest {

    public ConfirmRequest(String messageFromChannel, Node node, Map<String, NodeConfiguration> connectedNodes) throws GenericException {
        super(messageFromChannel, node, MessageType.CONFIRM_OK, MessageType.CONFIRM_ERR, connectedNodes, MessageType.RECORD_OK, MessageType.RECORD_ERR);
    }

    @Override
    protected String computeResponse(List<Transaction> transactions) throws GenericException {

        /**
         * [0] = guid
         * [1] = sender public key
         * [2] = recipient public key
         * [3] = amount
         * [4] = wallet transaction digest
         * [5] = timestamp
         * [6] = hash
         * [7] = firstnode:signature
         * [8] = secondnode:signature
         * [9] = thirdnode:signature
         */
        try {
            String[] messageBody = MessageDecoder.decodeBodyMessage(getBody());

            boolean firstNodeVerified = ChannelUtils.verifyNode.apply(messageBody[7], messageBody);
            boolean secondNodeVerified = ChannelUtils.verifyNode.apply(messageBody[8], messageBody);
            boolean thirdNodeVerified = ChannelUtils.verifyNode.apply(messageBody[9], messageBody);

            if (firstNodeVerified && secondNodeVerified && thirdNodeVerified) {

                KeystoreFileCredentials keystoreFileCredentials = new KeystoreFileCredentials(node.getKeystoreFilename(), node.getName(), node.getKeystorePassword(), node.getKeystorePassword());

                // Adding the confirmation signature to the client message to be added in the record.
                String bodyWithConfirmationSignature = new BodyBuilder(getBody())
                        .addData(signedPayload)
                        .build();

                ClientMessage clientMessage = new ClientMessage(MessageType.RECORD, bodyWithConfirmationSignature, node.loadPublicKey(), keystoreFileCredentials);
                String message = clientMessage.getMessage();

                List<CompletableFuture<String>> completableFutures = broadcast(message);

                if(completableFutures.size() >= Constants.NODES_TO_APPROVE) {
                    Transaction transaction = Transaction.from(bodyWithConfirmationSignature);
                    LedgerUtils.record(transaction, node.getName());
                }
            }

            completedSuccessfully = true;

            return "";
        }
        catch (IOException ex) {
            throw new GenericException(ErrorCodes.NETWORK_ERROR);
        }
    }
}
