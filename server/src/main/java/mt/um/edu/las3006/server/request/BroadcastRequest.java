package mt.um.edu.las3006.server.request;

import mt.um.edu.las3006.common.Constants;
import mt.um.edu.las3006.common.MessageType;
import mt.um.edu.las3006.common.builder.BodyBuilder;
import mt.um.edu.las3006.common.exceptions.ErrorCodes;
import mt.um.edu.las3006.common.exceptions.GenericException;
import mt.um.edu.las3006.common.utils.ChannelUtils;
import mt.um.edu.las3006.server.domain.Node;
import mt.um.edu.las3006.server.domain.NodeConfiguration;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public abstract class BroadcastRequest extends BaseRequest{

    private Map<String, NodeConfiguration> connectedNodes;

    private MessageType nodeSuccessResponseMessageType;

    private MessageType nodeErrorResponseMessageType;

    public BroadcastRequest(String request,
            Node node,
            MessageType successResponseMessage,
            MessageType errorResponseMessage,
            Map<String, NodeConfiguration> connectedNodes,
            MessageType nodeSuccessResponseMessageType,
            MessageType nodeErrorResponseMessageType) throws GenericException {
        super(request, node, successResponseMessage, errorResponseMessage);
        this.connectedNodes = connectedNodes;
        this.nodeSuccessResponseMessageType = nodeSuccessResponseMessageType;
        this.nodeErrorResponseMessageType = nodeErrorResponseMessageType;
    }

    protected List<CompletableFuture<String>> broadcast(String messageToBroadcast) throws GenericException {

        List<CompletableFuture<String>> connectedNodesFutures = connectedNodes.values()
                                                                                   .stream()
                                                                                   .map(nodeConfiguration -> this.sendMessageToNode(messageToBroadcast, nodeConfiguration))
                                                                                   .collect(Collectors.toList());

        CompletableFuture<Long> nodesVerified = CompletableFuture
                .allOf(connectedNodesFutures
                        .toArray(new CompletableFuture[connectedNodesFutures.size()]))
                .thenApply(messageResponses -> connectedNodesFutures.stream()
                                                     .map(CompletableFuture::join)
                                                     .collect(Collectors.toList()))
                .thenApply(messageTypes -> messageTypes.stream()
                                                       .filter(message -> message.startsWith(nodeSuccessResponseMessageType.name()))
                                                       .count());

        try {
            switch (nodeSuccessResponseMessageType) {
            case VERIFY_OK:
                if (nodesVerified.get().compareTo(Constants.NODES_TO_APPROVE) >= 0) {
                    return connectedNodesFutures;
                }
                else {
                    throw new GenericException(ErrorCodes.NODES_VERIFIED_NOT_ENOUGH);
                }
            case RECORD_OK:
                return connectedNodesFutures;
            default:
                throw new GenericException(ErrorCodes.INVALID_MESSAGE_BODY);
            }
        }
        catch (InterruptedException | ExecutionException ex) {
            throw new GenericException(ErrorCodes.NETWORK_ERROR);
        }
    }

    private CompletableFuture<String> sendMessageToNode(String message, NodeConfiguration nodeConfiguration) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                if (nodeConfiguration.trySend()) {
                    ChannelUtils.sendBytesToChannel(message, nodeConfiguration.getChannel());

                    String response = ChannelUtils.readBytesFromChannel(nodeConfiguration.getBuffer(), nodeConfiguration.getChannel());

                    nodeConfiguration.release();

                    return response;
                }
                else {
                    return new BodyBuilder(nodeErrorResponseMessageType.name())
                            .addData(ErrorCodes.TRANSACTION_IN_PROGRESS.getMessage())
                            .build();
                }
            }
            catch (IOException ex) {
                return nodeErrorResponseMessageType.name();
            }
        }).exceptionally(ex -> nodeErrorResponseMessageType.name());
    }
}
