package mt.um.edu.las3006.server.request;

import mt.edu.um.las3006.assignment.security.KeyLoader;
import mt.um.edu.las3006.common.MessageType;
import mt.um.edu.las3006.common.exceptions.GenericException;
import mt.um.edu.las3006.server.domain.Node;
import mt.um.edu.las3006.server.domain.Transaction;
import mt.um.edu.las3006.server.utils.LedgerUtils;

import java.util.List;

public class BalanceEnquiryRequest extends BaseRequest {

    public BalanceEnquiryRequest(String request, Node node) throws GenericException {
        super(request, node, MessageType.BALANCE_RESP, MessageType.BALANCE_ERR);
    }

    public String computeResponse(List<Transaction> transactions) throws GenericException {

        log.info("Computing the balance for public key user {}.", KeyLoader.encodePublicKey(getPublicKey()));

        Double balance = LedgerUtils.getBalance(transactions, getPublicKey());

        log.info("The balance for this client is {}.", balance);

        completedSuccessfully = true;

        return String.valueOf(balance);
    }
}
