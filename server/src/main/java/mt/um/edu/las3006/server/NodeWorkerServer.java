package mt.um.edu.las3006.server;

import mt.um.edu.las3006.common.Constants;
import mt.um.edu.las3006.common.MessageType;
import mt.um.edu.las3006.common.builder.OutgoingMessageBuilder;
import mt.um.edu.las3006.common.exceptions.GenericException;
import mt.um.edu.las3006.common.utils.ChannelUtils;
import mt.um.edu.las3006.server.domain.Node;
import mt.um.edu.las3006.server.domain.NodeConfiguration;
import mt.um.edu.las3006.server.request.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Map;

public class NodeWorkerServer {

    private final Logger log = LoggerFactory.getLogger(NodeWorkerServer.class);

    private Node node;

    private Selector selector;

    private volatile Map<String, NodeConfiguration> connectedNodes;

    public NodeWorkerServer(Node node, Map<String, NodeConfiguration> connectedNodes) {
        this.node = node;
        this.connectedNodes = connectedNodes;
    }

    public void start() throws IOException {

        selector = Selector.open();
        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);

        // retrieve server socket and bind to port
        serverChannel.socket().bind(new InetSocketAddress(node.getPort()));

        serverChannel.register(selector, SelectionKey.OP_ACCEPT);

        log.info("Server {} started on port {}.", node.getName(), node.getPort());

        while (true) {
            // Waiting for events...
            selector.select();

            // Work on selected keys.
            Iterator keys = selector.selectedKeys().iterator();
            while (keys.hasNext()) {

                SelectionKey key = (SelectionKey) keys.next();

                keys.remove();

                if (key.isValid() && key.isAcceptable()) {
                    this.accept(key);
                }
                if (key.isValid() && key.isReadable()) {
                    this.read(key);
                }
                if (!key.isValid()) {
                    log.warn("Selection key is invalid.");
                }
            }
        }

    }

    private void accept(SelectionKey key) throws IOException {

        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();

        // Accept the connection and make it non-blocking
        SocketChannel channel = serverSocketChannel.accept();
        Socket socket = channel.socket();
        channel.configureBlocking(false);

        // Register the new SocketChannel with our Selector, indicating
        // we'd like to be notified when there's data waiting to be read
        channel.register(this.selector, SelectionKey.OP_READ);

        SocketAddress remoteAddress = socket.getRemoteSocketAddress();
        log.info("Accepted connection from remote {} to local {}:{}.", remoteAddress, socket.getLocalAddress(), socket.getLocalPort());
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);

        String messageFromChannel = ChannelUtils.readBytesFromChannel(buffer, channel);
        String attachment;

        if ((messageFromChannel.isEmpty()) && !channel.isOpen()) {
            // todo properly remove the channel from map.
            this.connectedNodes.remove(channel);
            key.cancel();
        }
        else {

            try {
                BaseRequest baseRequest;

                switch (MessageType.valueOf(messageFromChannel.split(Constants.OUTGOING_SEPARATOR)[0])) {
                case BALANCE:
                    baseRequest = new BalanceEnquiryRequest(messageFromChannel, node);
                    break;
                case HISTORY:
                    baseRequest = new HistoryRequest(messageFromChannel, node);
                    break;
                case AUTH:
                    log.info("Nodes connected to so far: " + connectedNodes.keySet());
                    baseRequest = new AuthRequest(messageFromChannel, node, connectedNodes);
                    break;
                case VERIFY:
                    baseRequest = new VerificationRequest(messageFromChannel, node);
                    break;
                case CONFIRM:
                    baseRequest = new ConfirmRequest(messageFromChannel, node, connectedNodes);
                    break;
                case RECORD:
                    baseRequest = new RecordRequest(messageFromChannel, node);
                    break;
                default:
                    throw new UnsupportedOperationException("Unable to handle " + messageFromChannel.split(Constants.OUTGOING_SEPARATOR)[0]);
                }

                attachment = baseRequest.verifyAndCompute();
            }
            catch (GenericException ex) {
                attachment = new OutgoingMessageBuilder(ex.getErrorCodes().getMessage()).build();
            }

            ChannelUtils.sendBytesToChannel(attachment, channel);
        }
    }
}
