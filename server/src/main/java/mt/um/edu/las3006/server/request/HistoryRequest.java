package mt.um.edu.las3006.server.request;

import mt.edu.um.las3006.assignment.security.KeyLoader;
import mt.um.edu.las3006.common.MessageType;
import mt.um.edu.las3006.common.exceptions.GenericException;
import mt.um.edu.las3006.server.domain.Node;
import mt.um.edu.las3006.server.domain.Transaction;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class HistoryRequest extends BaseRequest {

    private final static String DATE_TIME_FORMAT = "yyyy-MM-dd kk:mm:ss";

    public HistoryRequest(String request, Node node) throws GenericException {
        super(request, node, MessageType.HISTORY_RESP, MessageType.HISTORY_ERR);
    }

    private enum Operation{
        CR,
        DR;
    }

    public String computeResponse(List<Transaction> transactions) throws GenericException {
        StringBuilder stringBuilder = new StringBuilder();

        transactions.stream()
                    .filter(transaction -> transaction.getRecipientPublicKey().equals(getPublicKey()) || transaction.getSenderPublicKey().equals(getPublicKey()))
                    .forEach(transaction -> {
            Operation operation = transaction.getRecipientPublicKey().equals(getPublicKey()) ? Operation.CR : Operation.DR;

            // Build the line for the transaction history.
            stringBuilder.append(String.format("%s %s %s %s\n",
                    Instant.ofEpochMilli(transaction.getTimestamp()).atZone(ZoneId.systemDefault()).toLocalDateTime().format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)),
                    operation,
                    transaction.getAmount(),
                    KeyLoader.encodePublicKey(operation == Operation.CR ? transaction.getSenderPublicKey() : transaction.getRecipientPublicKey())
                    ));
        });

        BalanceEnquiryRequest balanceEnquiryRequest = new BalanceEnquiryRequest(getRawRequest(), getNode());
        String balance = balanceEnquiryRequest.computeResponse(transactions);

        // Add the last line representing the balance as at date.
        stringBuilder.append(String.format("%s %s %s",
                LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT)),
                MessageType.BALANCE,
                balance
                ));

        completedSuccessfully = true;

        return stringBuilder.toString();
    }
}
