package mt.um.edu.las3006.server.domain;

import mt.edu.um.las3006.assignment.security.KeyLoader;
import mt.um.edu.las3006.common.builder.BodyBuilder;
import mt.um.edu.las3006.common.utils.MessageDecoder;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

public class Transaction implements Comparable<Transaction> {

    private final Long timestamp;

    private String hash;

    private final String guid;

    private final PublicKey senderPublicKey;

    private final PublicKey recipientPublicKey;

    private final String amount;

    private String senderAuthorisationSignature;

    private List<VerificationSignature> verificationSignatures;

    private String confirmationSignature;

    public Transaction(Long timestamp, String hash, String guid, PublicKey senderPublicKey, PublicKey recipientPublicKey, String amount, String senderAuthorisationSignature, String confirmationSignature) {
        this.timestamp = timestamp;
        this.hash = hash;
        this.guid = guid;
        this.senderPublicKey = senderPublicKey;
        this.recipientPublicKey = recipientPublicKey;
        this.amount = amount;
        this.senderAuthorisationSignature = senderAuthorisationSignature;
        this.verificationSignatures = new ArrayList<>();
        this.confirmationSignature = confirmationSignature;
    }

    public Transaction(Long timestamp, String hash, String guid, PublicKey senderPublicKey, PublicKey recipientPublicKey, String amount, String senderAuthorisationSignature, String verificationSignature1, String verificationSignature2, String verificationSignature3, String confirmationSignature) {
        this.timestamp = timestamp;
        this.hash = hash;
        this.guid = guid;
        this.senderPublicKey = senderPublicKey;
        this.recipientPublicKey = recipientPublicKey;
        this.amount = amount;
        this.senderAuthorisationSignature = senderAuthorisationSignature;

        verificationSignatures = new ArrayList<>();
        VerificationSignature verificationSignatureObj1 = new VerificationSignature(verificationSignature1);
        VerificationSignature verificationSignatureObj2 = new VerificationSignature(verificationSignature2);
        VerificationSignature verificationSignatureObj3 = new VerificationSignature(verificationSignature3);
        verificationSignatures.add(verificationSignatureObj1);
        verificationSignatures.add(verificationSignatureObj2);
        verificationSignatures.add(verificationSignatureObj3);

        this.confirmationSignature = confirmationSignature;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public String getHash() {
        return hash;
    }

    public String getGuid() {
        return guid;
    }

    public PublicKey getSenderPublicKey() {
        return senderPublicKey;
    }

    public PublicKey getRecipientPublicKey() {
        return recipientPublicKey;
    }

    public String getAmount() {
        return amount;
    }

    public void setSenderAuthorisationSignature(String senderAuthorisationSignature){
        this.senderAuthorisationSignature = senderAuthorisationSignature;
    }

    public String getSenderAuthorisationSignature() {
        return senderAuthorisationSignature;
    }

    public List<VerificationSignature> getVerificationSignatures() {
        return verificationSignatures;
    }

    public String getConfirmationSignature() {
        return confirmationSignature;
    }

    public void setVerificationSignatures(List<VerificationSignature> verificationSignatures) {
        this.verificationSignatures = verificationSignatures;
    }

    @Override
    public int compareTo(Transaction transaction2)
    {
        return this.timestamp.compareTo(transaction2.getTimestamp());
    }

    public void setConfirmationSignature(String confirmationSignature) {
        this.confirmationSignature = confirmationSignature;
    }

    @Override
    public String toString() {
        return new BodyBuilder(String.valueOf(timestamp))
                .addData(hash)
                .addData(guid)
                .addData(KeyLoader.encodePublicKey(senderPublicKey))
                .addData(KeyLoader.encodePublicKey(recipientPublicKey))
                .addData(amount)
                .addData(senderAuthorisationSignature)
                .addData(verificationSignatures.get(0).toString())
                .addData(verificationSignatures.get(1).toString())
                .addData(verificationSignatures.get(2).toString())
                .addData(confirmationSignature + "\n")
                .build();
    }

    public static Transaction from(String body){
        String[] bodyMessage = MessageDecoder.decodeBodyMessage(body);

        /**
         * [0] = guid
         * [1] = sender public key
         * [2] = recipient public key
         * [3] = amount
         * [4] = wallet transaction digest
         * [5] = timestamp
         * [6] = hash
         * [7] = firstnode:signature
         * [8] = secondnode:signature
         * [9] = thirdnode:signature
         * [10] = confirmationSignature
         */
        return new Transaction(
                Long.valueOf(bodyMessage[5]),
                bodyMessage[6],
                bodyMessage[0],
                KeyLoader.decodePublicKey(bodyMessage[1]),
                KeyLoader.decodePublicKey(bodyMessage[2]),
                bodyMessage[3],
                bodyMessage[4],
                bodyMessage[7],
                bodyMessage[8],
                bodyMessage[9],
                bodyMessage.length > 10 ? bodyMessage[10] : null);
    }
}
