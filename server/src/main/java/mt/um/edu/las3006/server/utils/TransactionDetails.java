package mt.um.edu.las3006.server.utils;

public class TransactionDetails {

    private final String hashValue;

    private final String nodeSignature;

    private final long timestamp;

    private final String amount;

    public TransactionDetails(String hashValue, String nodeSignature, long timestamp, String amount) {
        this.hashValue = hashValue;
        this.nodeSignature = nodeSignature;
        this.timestamp = timestamp;
        this.amount = amount;
    }

    public String getHashValue() {
        return hashValue;
    }

    public String getNodeSignature() {
        return nodeSignature;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getAmount() {
        return amount;
    }
}
