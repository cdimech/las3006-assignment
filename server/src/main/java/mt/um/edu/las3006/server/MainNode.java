package mt.um.edu.las3006.server;

import mt.um.edu.las3006.server.domain.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class MainNode {

    private static final Logger log = LoggerFactory.getLogger(MainNode.class);

    public static void main(String[] args) throws IOException {

        // Fail if not all arguments are specified.
        if (args.length < 2) {
            log.error("You must specify all 2 parameters: node name and port to listen to.");
            System.exit(-1);
        }

        Node node = new Node(args[0], Integer.valueOf(args[1]));

        NodeWorkerClient nodeWorkerClient = new NodeWorkerClient(node);
        nodeWorkerClient.start();

        NodeWorkerServer nodeWorker = new NodeWorkerServer(node, nodeWorkerClient.getNodesConnected());
        nodeWorker.start();
    }
}
