package mt.um.edu.las3006.server;

import mt.um.edu.las3006.common.utils.FileUtils;
import mt.um.edu.las3006.server.domain.Node;
import mt.um.edu.las3006.server.domain.NodeConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class NodeWorkerClient extends Thread {

    private final Logger log = LoggerFactory.getLogger(NodeWorkerClient.class);

    private final Node node;

    private volatile ConcurrentMap<String, NodeConfiguration> nodesConnected;

    public NodeWorkerClient(Node node) {
        this.node = node;
        this.nodesConnected = new ConcurrentHashMap<>();
        // todo
        // setDeamon?
    }

    private List<NodeDiscovery> discover(){
        List<NodeDiscovery> nodesToDiscover = FileUtils.readIpsFromFile()
                                                       .entrySet()
                                                       .stream()
                                                       .filter(ip -> !ip.getKey().equals(this.node.getName())) // do not try to connect to self.
                                                       .map(x -> new NodeDiscovery(x.getKey(), x.getValue(), nodesConnected))
                                                       .collect(Collectors.toList());

        log.info("Nodes to discover = {}.", nodesToDiscover);

        return nodesToDiscover;
    }

    @Override
    public void run() {
        CompletableFuture.allOf(discover()
                .stream()
                .map(node -> CompletableFuture.supplyAsync(node::call))
                .toArray(CompletableFuture<?>[]::new))
                         .join();

        log.info("Nodes connected to => {}.", nodesConnected.keySet().toString());
    }

    protected Map<String, NodeConfiguration> getNodesConnected() {
        return nodesConnected;
    }
}
