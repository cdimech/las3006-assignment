## Synopsis

There are 3 main modules in this project:
- client
- common-library
- server

The only executable classes are MainWallet in the client module and MainNode in the server module.

## Running the executables

There are 2 different ways how one can run the executables:

### Using assembly plugin

First, you have to run the following command:

`mvn clean install`

after which you can go to the target folder and run:

####Client
`java -jar client/target/client-1.0-SNAPSHOT-jar-with-dependencies.jar <wallet user> <node name> <operation>`

####Server
`java -jar server/target/server-1.0-SNAPSHOT-jar-with-dependencies.jar <node name> <port>`

### Using exec plugin

First, you have to go to the respective directory that has the module you would like to run, so either `/client` or `/server`.

In order to run the client, you need the following command:

`mvn exec:java -DkeyAlias=alice -DnodeName=node1 -Doperation=balance`

In case you are running the server, you can use the following command:

`mvn exec:java -DnodeName=node1 -Dport=12111`

## Author

Claire Borg Dimech

cl.dimech@gmail.com

33890G