package mt.um.edu.las3006.client.worker;

import mt.um.edu.las3006.client.domain.Wallet;
import mt.um.edu.las3006.common.MessageType;
import mt.um.edu.las3006.common.domain.ClientMessage;
import mt.um.edu.las3006.common.exceptions.GenericException;
import mt.um.edu.las3006.common.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Optional;

public class WalletWorker {

    private final Logger log = LoggerFactory.getLogger(WalletWorker.class);

    private final Wallet wallet;

    private final ByteBuffer buffer = ByteBuffer.allocate(1024);

    public WalletWorker(Wallet wallet) {
        this.wallet = wallet;
    }

    public void start() {

        Optional<String> ipOptional = FileUtils.ipLookupByNode(wallet.getNodeName());

        ipOptional.ifPresent(ip -> {

            log.info("IP for {} found = {}", wallet.getNodeName(), ip);

            String[] ipArray = ip.split(":");
            String host = ipArray[0];
            int port = Integer.valueOf(ipArray[1]);

            try {
                establishConnection(host, port);
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        });
    }

    private void establishConnection(String host, int port) throws IOException {

        log.info("Attempting to connect to server on {}:{}...", host, port);

        Selector selector = Selector.open();
        SocketChannel channel = SocketChannel.open();
        channel.configureBlocking(false);

        channel.register(selector, SelectionKey.OP_CONNECT);
        channel.connect(new InetSocketAddress(host, port));

        try {
            while (true) {
                // Waiting for events.
                selector.select(60000);

                Iterator keys = selector.selectedKeys().iterator();

                while (keys.hasNext()) {
                    SelectionKey key = (SelectionKey) keys.next();

                    keys.remove();

                    // Since this will only behave as a client, it will only connect to  the server, read and write data.
                    if (key.isValid() && key.isConnectable()) {
                        this.connect(key);
                    }
                    if (key.isValid() && key.isWritable()) {
                        this.write(key);
                    }
                    if (key.isValid() && key.isReadable()) {
                        this.read(key);
                    }
                }
            }
        }
        catch (GenericException ex) {
            log.error("An error has occurred - {}", ex.getErrorCodes().getMessage());
            ex.printStackTrace();
        }
    }

    private void connect(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();

        try {
            if (channel.finishConnect()) {
                log.info("Connected to server.");
            }
        }
        catch (IOException e) {
            key.cancel();
            return;
        }

        key.interestOps(SelectionKey.OP_WRITE);
    }

    private void write(SelectionKey key) throws IOException, GenericException {
        SocketChannel channel = (SocketChannel) key.channel();

        WalletSender sender = new WalletSender(wallet, channel, key.attachment());
        sender.send();

        // Set the interest ops to READ to receive the response from the server.
        key.interestOps(SelectionKey.OP_READ);
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();

        try {
            WalletReceiver receiver = new WalletReceiver(channel, buffer, wallet);
            ClientMessage receive = receiver.receive();

            if (receive != null && MessageType.CONFIRM.equals(receive.getHeader())) {
                key.attach(receive);
                // change message type.
                wallet.setMessageType(MessageType.CONFIRM);

                key.interestOps(SelectionKey.OP_WRITE);
            }
            else {
                log.info("Closing connection for wallet.");
                // Close the channel since no more communication is needed beyond this point.
                channel.close();
                key.cancel();
                System.exit(0);
            }
        }
        catch (Exception ex) {
            log.error("An exception has occurred - ", ex);
            key.cancel();
        }
    }
}
