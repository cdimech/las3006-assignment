package mt.um.edu.las3006.client.worker;

import mt.um.edu.las3006.client.domain.Wallet;
import mt.um.edu.las3006.common.MessageType;
import mt.um.edu.las3006.common.domain.ClientMessage;
import mt.um.edu.las3006.common.exceptions.GenericException;
import mt.um.edu.las3006.common.utils.ChannelUtils;
import mt.um.edu.las3006.common.utils.MessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class WalletReceiver {

    private final Logger log = LoggerFactory.getLogger(WalletReceiver.class);

    private final SocketChannel channel;

    private final ByteBuffer buffer;

    private final Wallet wallet;

    public WalletReceiver(SocketChannel channel, ByteBuffer buffer, Wallet wallet) {
        this.channel = channel;
        this.buffer = buffer;
        this.wallet = wallet;
    }

    public ClientMessage receive() throws IOException, GenericException {

        String received = ChannelUtils.readBytesFromChannel(buffer, channel);

        ClientMessage clientMessageReceived = ClientMessage.from(received);

        switch (clientMessageReceived.getHeader()) {
        case BALANCE_RESP:
            log.info("Balance available: {}", clientMessageReceived.getBody());
            break;
        case HISTORY_RESP:
            log.info("History: \n{}", clientMessageReceived.getBody());
            break;
        case AUTH_OK:
            log.info("Received a successful auth for the transfer.");
            String[] body = MessageDecoder.decodeBodyMessage(clientMessageReceived.getBody());

            // todo this can be improved using the Predicate andThen (see worksheets class ProductChecker
            boolean verifiedByFirstNode = ChannelUtils.verifyNode.apply(body[7], body);
            boolean verifiedBySecondNode = ChannelUtils.verifyNode.apply(body[8], body);
            boolean verifiedByThirdNode = ChannelUtils.verifyNode.apply(body[9], body);

            if(verifiedByFirstNode && verifiedBySecondNode && verifiedByThirdNode){
                return new ClientMessage(MessageType.CONFIRM,
                        clientMessageReceived.getBody(),
                        wallet.getWalletPublicKey(),
                        clientMessageReceived.getFooter());
            }
            break;
        case CONFIRM_OK:
            break;
        case BALANCE_ERR:
        case HISTORY_ERR:
        case AUTH_ERR:
        case CONFIRM_ERR:
            log.error("ERROR FROM NODE - {}.", clientMessageReceived.getBody());
            break;
        }

        return new ClientMessage(clientMessageReceived.getHeader(), null, null, "");
    }
}
