package mt.um.edu.las3006.client.worker;

import mt.edu.um.las3006.assignment.security.KeyLoader;
import mt.edu.um.las3006.assignment.security.SignatureBuilder;
import mt.um.edu.las3006.client.domain.Wallet;
import mt.um.edu.las3006.common.MessageType;
import mt.um.edu.las3006.common.builder.BodyBuilder;
import mt.um.edu.las3006.common.domain.ClientMessage;
import mt.um.edu.las3006.common.exceptions.GenericException;
import mt.um.edu.las3006.common.utils.ChannelUtils;
import mt.um.edu.las3006.common.utils.FormatUtils;

import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.util.UUID;

public class WalletSender {

    private final Wallet wallet;

    private final SocketChannel channel;

    private ClientMessage attachment;

    public WalletSender(Wallet wallet, SocketChannel channel, Object attachment) {
        this.wallet = wallet;
        this.channel = channel;
        if (attachment != null && attachment instanceof ClientMessage) {
            this.attachment = (ClientMessage) attachment;
        }
    }

    public void send() throws IOException {

        MessageType header = wallet.getMessageType();

        String body = getBodyFromMessageType();

        String messageToSend = null;

        try {
            ClientMessage clientMessage = new ClientMessage(header, body, wallet.getWalletPublicKey(), wallet.getWalletPrivateKey());
            messageToSend = clientMessage.getMessage();
        }
        catch (GenericException ex) {
            messageToSend = ex.getErrorCodes().getMessage();
        }
        finally {
            ChannelUtils.sendBytesToChannel(messageToSend, channel);
        }
    }

    /**
     * The body is empty for BALANCE and HISTORY. It is overloaded only when a TRANSFER is going to be sent.
     *
     * @return the appropriate message body for the given message type.
     */
    private String getBodyFromMessageType() {
        switch (wallet.getMessageType()) {
        case BALANCE:
        case HISTORY:
            return "";
        case AUTH:
            return prepareAuthMessageBody();
        case CONFIRM:
            return attachment.getBody();
        default:
            return null;
        }
    }

    private String prepareAuthMessageBody() {

        String guid = UUID.randomUUID().toString();

        String senderPublicKey = KeyLoader.encodePublicKey(wallet.getWalletPublicKey());

        String transactionDigest = new SignatureBuilder(wallet.getWalletPrivateKey())
                .addData(guid)
                .addData(senderPublicKey)
                .addData(wallet.getRecipientPublicKey())
                .addData(FormatUtils.getFormattedAmount(wallet.getAmount()))
                .sign();

        // Overload the body with the TX details.
        return new BodyBuilder(guid)
                .addData(senderPublicKey)
                .addData(wallet.getRecipientPublicKey())
                .addData(FormatUtils.getFormattedAmount(wallet.getAmount()))
                .addData(transactionDigest)
                .build();
    }
}
