package mt.um.edu.las3006.client;

import mt.edu.um.las3006.assignment.security.KeyLoader;
import mt.um.edu.las3006.client.domain.Wallet;
import mt.um.edu.las3006.client.worker.WalletWorker;
import mt.um.edu.las3006.common.Constants;
import mt.um.edu.las3006.common.MessageType;
import mt.um.edu.las3006.common.exceptions.ErrorCodes;
import mt.um.edu.las3006.common.exceptions.GenericException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Paths;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * Main class to start the wallet client and accept arguments.
 */
public class MainWallet {

    private static final Logger log = LoggerFactory.getLogger(MainWallet.class);

    public static void main(String[] args) throws GenericException {

        // Fail if not all arguments are specified.
        if (args.length < 3) {
            log.error("You must specify all 3 parameters: user, node name and operation (balance/history/transfer)");
            System.exit(-1);
        }

        String keyAlias = args[0];
        String nodeName = args[1];
        String operation = args[2];

        // Check that the operation inputted is valid.
        if (!Wallet.messages.keySet().contains(operation)) {
            log.error("Invalid operation type {}.", operation);
            System.exit(-1);
        }

        java.io.Console console = System.console();
        String keystorePassword = new String(console.readPassword("Keystore password: "));
//        String keystorePassword = "alice1234";

        String recipientPublicKey = null;
        Double amount = 0d;

        if (MessageType.AUTH.equals(Wallet.messages.get(operation))) {
//            recipientPublicKey = "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAELpFkdsXJF9Sw1nexEUq6etsVw2kJFDBG+AeuPA/wCdvOO/J4fJqo/sRWMfSmESA+AJfzJZnxWhU4OSCO5U0rag==";
//            amount = 100d;
            recipientPublicKey = console.readLine("Recipient public key: ");
            amount = Double.valueOf(console.readLine("Amount for the transfer: "));
            if (amount < 0) {
                throw new NumberFormatException("The amount inputted is invalid - should be greater than 0");
            }
        }

        String keyStoreFilename = String.format("%s.pfx", keyAlias);

        try {
            PublicKey walletPublicKey = KeyLoader.loadPublicKey(Paths.get(Constants.SECURITY_DIRECTORY,
                    keyStoreFilename), keyAlias, keystorePassword);

            PrivateKey walletPrivateKey = KeyLoader.loadPrivateKey(Paths.get(Constants.SECURITY_DIRECTORY,
                    keyStoreFilename), keyAlias, keystorePassword, keystorePassword);

            Wallet wallet = new Wallet(keyAlias, nodeName, operation, walletPublicKey, walletPrivateKey, recipientPublicKey, amount);
            WalletWorker walletWorker = new WalletWorker(wallet);
            walletWorker.start();
        }
        catch (Exception ex) {
            log.error(ErrorCodes.UNKNOWN_PARTICIPANT.getMessage(), ex);
            System.exit(-1);
        }

    }
}
