package mt.um.edu.las3006.client.domain;

import com.google.common.collect.ImmutableMap;
import mt.um.edu.las3006.common.MessageType;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Map;

public class Wallet {

    public static final Map<String, MessageType> messages = ImmutableMap.of(
            "balance", MessageType.BALANCE,
            "history", MessageType.HISTORY,
            "transfer", MessageType.AUTH);

    private final String keyAlias;

    private final String nodeName;

    private final PublicKey walletPublicKey;

    private final PrivateKey walletPrivateKey;

    private String recipientPublicKey;

    private Double amount;

    private MessageType messageType;

    public Wallet(String keyAlias, String nodeName, String operation, PublicKey walletPublicKey, PrivateKey walletPrivateKey) {
        this.keyAlias = keyAlias;
        this.nodeName = nodeName;
        this.messageType = messages.get(operation);
        this.walletPrivateKey = walletPrivateKey;
        this.walletPublicKey = walletPublicKey;
    }

    public Wallet(String keyAlias, String nodeName, String operation, PublicKey walletPublicKey, PrivateKey walletPrivateKey, String recipientPublicKey, Double amount) {
        this.keyAlias = keyAlias;
        this.nodeName = nodeName;
        this.messageType = messages.get(operation);
        this.recipientPublicKey = recipientPublicKey;
        this.amount = amount;
        this.walletPrivateKey = walletPrivateKey;
        this.walletPublicKey = walletPublicKey;
    }

    public void setRecipientPublicKey(String recipientPublicKey) {
        this.recipientPublicKey = recipientPublicKey;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getKeyAlias() {
        return keyAlias;
    }

    public String getNodeName() {
        return nodeName;
    }

    public PublicKey getWalletPublicKey() {
        return walletPublicKey;
    }

    public PrivateKey getWalletPrivateKey() {
        return walletPrivateKey;
    }

    public String getRecipientPublicKey() {
        return recipientPublicKey;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public MessageType getMessageType(){
        return messageType;
    }

    public String getKeystoreFilename(){
        return String.format("%s.pfx", keyAlias);
    }

    public Double getAmount() {
        return amount;
    }
}
